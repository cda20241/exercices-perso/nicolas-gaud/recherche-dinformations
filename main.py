import random

people = [['Pascal', 'ROSE', 43],
          ['Mickaël', 'FLEUR', 29],
          ["Henri", "TULIPE", 35],
          ["Michel", "FRAMBOISE", 35],
          ["Arthur", "PETALE", 65],
          ["Michel", "POLLEN", 50],
          ["Michel", "FRAMBOISE", 42],
          ["Michel", "FRAMBOISE", 42],
          ["Michel", "FRAMBOISE", 42],
          ["Michel", "FRAMBOISE", 42]]


FIRST_NAME_INDEX = 0
NAME_INDEX = 1
AGE_INDEX = 2

random.shuffle(people)


def lovecamp():
    """
    :param: none
    This function tests if person name is Michel or its age is 50 or more
    and shows a message depending on this test
    :return: none
    """
    personvar = [person for person in people if person[FIRST_NAME_INDEX] == 'Michel' or person[AGE_INDEX] >= 50]
    if personvar != []:
        print('\nex 1 ====================')
        print('On a dans la liste quelqu\'un qui adore le camping !')
        print('\nex 2 + pour aller plus loin ====================')
        for i in range(len(personvar)):
            print('C\'est formidable (' + personvar[i][FIRST_NAME_INDEX] + ', ' + personvar[i][NAME_INDEX] + ', '
                  + str(personvar[i][AGE_INDEX]) + ') adore le camping !')
    else:
        print('\nex 1 ====================')
        print('Personne dans la liste n\'aime le camping !')
        print('\nex 2 + pour aller plus loin ====================')
        print('Personne dans la liste n\'aime le camping !')

def checkletters():
    """
    :param:none
    This function tests if a person's first name and name are equal and show a message depending on this test
    :return: none
    """
    message = ''
    isequal = False
    for person in people:
        message = ''
        if len(person[FIRST_NAME_INDEX]) == len(person[NAME_INDEX]):
            message = 'C\'est formidable (' + person[FIRST_NAME_INDEX] + ', ' + person[NAME_INDEX] + ', ' \
            + str(person[AGE_INDEX]) + ') a ' + str(len(person[NAME_INDEX])) + ' lettres dans son nom et son prénom !'
            isequal = True
            print(message)
    if not isequal:
            raise Exception('Personne n\'a le même nombre de lettres dans son nom et son prénom !')
    print(message)

def compareperson(listpeople, personindex):
    """"
    :param: A list of people and an index
    This function tests if there are surnames, names or ages equal depending on the index
    and shows a message depending on these tests
    :return:none
    """
    result = []
    message = ''
    for person in listpeople:
        person: list
        for comparedperson in listpeople:
            comparedperson: list
            if person != comparedperson and person[personindex] == comparedperson[personindex] and person not in result:
                result.append(person)
    if result:
        message = 'C\'est formidable ' + str(result[0])
        for i in range(len(result)):
            if i != 0:
                message += ' et ' + str(result[i])
        if personindex == FIRST_NAME_INDEX:
            message += ' ont des prénoms identiques'
        elif personindex == NAME_INDEX:
            message += ' ont des noms identiques'
        elif personindex == AGE_INDEX:
            message += ' ont des ages identiques'
    else:
        if personindex == FIRST_NAME_INDEX:
            message += 'Personne n\'a de prenom identiques'
        elif personindex == NAME_INDEX:
            message += 'Personne n\'a de nom identiques'
        elif personindex == AGE_INDEX:
            message += 'Personne n\'a d\'age identiques'
    print(message)


lovecamp()
print('\nex 3 ====================')
checkletters()
print('\nex 4 ====================')
compareperson(people, FIRST_NAME_INDEX)
print('\nex 5 ====================')
compareperson(people, NAME_INDEX)
print('\nex 6 ====================')
compareperson(people, AGE_INDEX)
